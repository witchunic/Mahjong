﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System.Linq;

public class DropGUI : MonoBehaviour, IDropHandler, IPointerEnterHandler,IPointerExitHandler {

    public void OnDrop(PointerEventData eventData)
    {
        if (PlayerBase.instance.CurrentPlayer == 0 && game_system.instance.State == game_system.GameState.playerTurn)
        {
            if ((game_system.instance.kostyl.Contains(eventData.pointerDrag.name) && !PlayerBase.instance.IsRiichi(0)) || (PlayerBase.instance.IsRiichi(0) && eventData.pointerDrag.name == "14"))
  {
                eventData.pointerDrag.GetComponent<TileGUI>().OnEndDrag(eventData);
                tile_system.instance.DiscardByButton(System.Convert.ToInt32(eventData.pointerDrag.gameObject.name));
            }
        }
    }

    public void OnPointerEnter (PointerEventData eventData)
    {

    }

    public void OnPointerExit(PointerEventData eventData)
    {

    }
}
